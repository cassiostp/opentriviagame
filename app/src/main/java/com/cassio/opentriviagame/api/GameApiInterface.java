package com.cassio.opentriviagame.api;

import com.cassio.opentriviagame.entity.Game;

import retrofit2.Call;
import retrofit2.http.GET;

public interface GameApiInterface {
    @GET("api.php?amount=5")
    Call<Game> getGame();
}
