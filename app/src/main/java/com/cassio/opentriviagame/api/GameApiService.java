package com.cassio.opentriviagame.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GameApiService {
    private final static String BASE_URL = "https://opentdb.com/";
    private static Retrofit retrofit = null;
    private static GameApiService gameApiService;
    private static GameApiInterface gameApiInterface;

    private GameApiService() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        gameApiInterface = retrofit.create(GameApiInterface.class);
    }

    public static GameApiService getInstance() {
        if (gameApiService == null) {
            gameApiService = new GameApiService();
        }
        return gameApiService;
    }

    public GameApiInterface getGameApiInterface() {
        return gameApiInterface;
    }
}
