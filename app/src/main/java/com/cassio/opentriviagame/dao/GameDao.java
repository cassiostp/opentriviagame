package com.cassio.opentriviagame.dao;

import com.cassio.opentriviagame.entity.Game;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface GameDao {
    @Query("SELECT * FROM game ORDER BY score DESC LIMIT 10")
    LiveData<List<Game>> getBestGames();

    @Query("SELECT * FROM game WHERE player_name = :name AND isComplete = 0")
    LiveData<Game> getCurrentGame(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Game game);
}
