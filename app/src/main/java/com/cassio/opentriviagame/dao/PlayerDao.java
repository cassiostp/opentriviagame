package com.cassio.opentriviagame.dao;

import com.cassio.opentriviagame.entity.Player;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface PlayerDao {
    @Query("SELECT * FROM player")
    LiveData<List<Player>> getAll();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Player player);

}
