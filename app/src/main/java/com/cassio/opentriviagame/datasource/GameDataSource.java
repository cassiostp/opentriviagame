package com.cassio.opentriviagame.datasource;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cassio.opentriviagame.api.GameApiInterface;
import com.cassio.opentriviagame.api.GameApiService;
import com.cassio.opentriviagame.dao.GameDao;
import com.cassio.opentriviagame.db.AppDatabase;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.repository.PlayerRepository;

import java.util.List;

import androidx.lifecycle.LiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameDataSource {
    private GameApiService gameApiService = GameApiService.getInstance();

    public LiveData<List<Game>> getBestGames(Context context) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        return appDatabase.gameDao().getBestGames();
    }

    public LiveData<Game> getCurrentGame(Context context, String player_name) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        return appDatabase.gameDao().getCurrentGame(player_name);
    }

    public void insertGame(Context context, Game game) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new insertAsyncTask(appDatabase.gameDao()).execute(game);
    }

    private static class insertAsyncTask extends AsyncTask<Game, Void, Void> {

        private GameDao mAsyncTaskDao;

        insertAsyncTask(GameDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Game... games) {
            mAsyncTaskDao.insert(games[0]);
            return null;
        }
    }

    public void setNewGame(final Context context) {
        GameApiInterface apiInterface = gameApiService.getGameApiInterface();
        apiInterface.getGame().enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
                Game game = response.body();
                game.setPlayer_name(PlayerRepository.currentPlayer.getName());
                appDatabase.gameDao().insert(game);
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {
                // return local data if there is any or return connection error
                Log.d("RETROFITFAIL", t.getMessage());
            }
        });
    }
}
