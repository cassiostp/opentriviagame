package com.cassio.opentriviagame.datasource;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cassio.opentriviagame.dao.GameDao;
import com.cassio.opentriviagame.db.AppDatabase;
import com.cassio.opentriviagame.entity.Game;

import java.util.List;

import androidx.lifecycle.LiveData;

public class GameLocalDataSource {
    public LiveData<List<Game>> getBestGames(Context context) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        return appDatabase.gameDao().getBestGames();
    }

    public LiveData<Game> getCurrentGame(Context context, String player_name) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        return appDatabase.gameDao().getCurrentGame(player_name);
    }

    public void insertGame(Context context, Game game) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new insertAsyncTask(appDatabase.gameDao()).execute(game);
    }

    private static class insertAsyncTask extends AsyncTask<Game, Void, Void> {

        private GameDao mAsyncTaskDao;

        insertAsyncTask(GameDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Game... games) {
            mAsyncTaskDao.insert(games[0]);
            return null;
        }
    }
}
