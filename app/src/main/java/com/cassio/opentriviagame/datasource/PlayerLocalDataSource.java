package com.cassio.opentriviagame.datasource;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.cassio.opentriviagame.dao.PlayerDao;
import com.cassio.opentriviagame.db.AppDatabase;
import com.cassio.opentriviagame.entity.Player;

import java.util.List;

import androidx.lifecycle.LiveData;

public class PlayerLocalDataSource {
    public LiveData<List<Player>> getPlayers(Context context) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        return appDatabase.playerDao().getAll();
    }

    public void insertPlayer(Context context, Player player) {
        AppDatabase appDatabase = AppDatabase.getAppDatabase(context);
        new insertAsyncTask(appDatabase.playerDao()).execute(player);
    }

    private static class insertAsyncTask extends AsyncTask<Player, Void, Void> {

        private PlayerDao mAsyncTaskDao;

        insertAsyncTask(PlayerDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Player... players) {
            mAsyncTaskDao.insert(players[0]);
            return null;
        }
    }
}
