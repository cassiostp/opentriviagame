package com.cassio.opentriviagame.db;

import android.content.Context;

import com.cassio.opentriviagame.dao.GameDao;
import com.cassio.opentriviagame.dao.PlayerDao;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.entity.Player;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {Player.class, Game.class}, version = 8)
public abstract class AppDatabase extends RoomDatabase {
    public static final String DATABASE_NAME = "otg_db";
    public static AppDatabase appDatabase;
    public abstract PlayerDao playerDao();
    public abstract GameDao gameDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (appDatabase == null) {
            appDatabase = Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    DATABASE_NAME
            ).allowMainThreadQueries().fallbackToDestructiveMigration().build();
        }
        return appDatabase;
    }

    public static void destroyAppDatabaseInstance() {
        appDatabase = null;
    }
}
