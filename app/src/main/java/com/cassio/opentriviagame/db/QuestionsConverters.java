package com.cassio.opentriviagame.db;

import android.util.Log;

import com.cassio.opentriviagame.entity.Question;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.room.TypeConverter;

public class QuestionsConverters {
    @TypeConverter
    public static ArrayList<Question> fromString(String value) {
        Type listType = new TypeToken<ArrayList<Question>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }

    @TypeConverter
    public static String fromArrayList(ArrayList<Question> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }
}
