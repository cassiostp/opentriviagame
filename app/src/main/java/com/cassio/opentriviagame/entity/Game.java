package com.cassio.opentriviagame.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cassio.opentriviagame.db.QuestionsConverters;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

@Entity(foreignKeys = @ForeignKey(
        entity = Player.class, parentColumns = "name",
        childColumns = "player_name", onDelete = ForeignKey.CASCADE))
public class Game implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int uid;
    @Ignore
    @SerializedName("response_code")
    @Expose
    private Integer responseCode;
    @TypeConverters(QuestionsConverters.class)
    @ColumnInfo
    @SerializedName("results")
    @Expose
    private ArrayList<Question> questions;
    @ColumnInfo
    private String player_name;
    @ColumnInfo
    private Boolean isComplete = false;
    @ColumnInfo
    private int score;

    public Game() {
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getUid() { return uid; }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }

    public String getPlayer_name() { return player_name; }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public Boolean getComplete() {
        return isComplete;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}
