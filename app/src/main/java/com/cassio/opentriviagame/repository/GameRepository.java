package com.cassio.opentriviagame.repository;

import android.content.Context;

import com.cassio.opentriviagame.datasource.GameLocalDataSource;
import com.cassio.opentriviagame.datasource.GameDataSource;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.entity.Question;

import java.util.List;

import androidx.lifecycle.LiveData;

public class GameRepository {
    private GameDataSource gameDataSource = new GameDataSource();
    private GameLocalDataSource gameLocalDataSource = new GameLocalDataSource();
    private static LiveData<Game> localGame;

    public LiveData<List<Game>> getBestGames(Context context) {
        return gameDataSource.getBestGames(context);
    }

    public LiveData<Game> getCurrentGame(Context context) {
        localGame = gameDataSource.getCurrentGame(
                context, PlayerRepository.currentPlayer.getName());
        return localGame;
    }

    public void setNewGame(Context context) {
        gameDataSource.setNewGame(context);
    }

    public void saveGame(Context context, Game game) {
        gameDataSource.insertGame(context, game);
    }
}
