package com.cassio.opentriviagame.repository;

import android.content.Context;

import com.cassio.opentriviagame.datasource.PlayerLocalDataSource;
import com.cassio.opentriviagame.entity.Player;

import java.util.List;

import androidx.lifecycle.LiveData;

public class PlayerRepository {
    private PlayerLocalDataSource playerLocalDataSource = new PlayerLocalDataSource();
    public static Player currentPlayer;

    public LiveData<List<Player>> getPlayers(Context context) {
        return playerLocalDataSource.getPlayers(context);
    }

    public void insertPlayer(Context context, Player player) {
        currentPlayer = player;
        playerLocalDataSource.insertPlayer(context, player);
    }
}
