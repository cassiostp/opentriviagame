package com.cassio.opentriviagame.ui;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cassio.opentriviagame.R;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.entity.Question;
import com.cassio.opentriviagame.utilities.AnswersUtil;
import com.cassio.opentriviagame.viewmodel.GameViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

public class GameFragment extends Fragment {

    private View view;
    private Game currentGame;
    private int questionIndex = 0;
    private TextView questionText;
    private ArrayList<Button> answers = new ArrayList<>();
    private ContentLoadingProgressBar progress;

    private GameViewModel viewModel;

    public GameFragment() {
        // Required empty public constructor
    }

    private final Observer<Game> observer = new Observer<Game>() {
        @Override
        public void onChanged(Game game) {
            if (game == null) viewModel.setNewGame(GameFragment.this.getContext());
            else {
                currentGame = game;
                progress.setVisibility(View.GONE);
                Question currentQuestion = currentGame.getQuestions().get(questionIndex);
                questionText.setText(currentQuestion.getQuestion());
                List<String> answersText = AnswersUtil.orderAnswers(currentQuestion);
                for (int i = 0; i < 4; i++) {
                    if (i < answersText.size()) {
                        answers.get(i).setText(Html.fromHtml(answersText.get(i)));
                        answers.get(i).setVisibility(View.VISIBLE);
                        if (i == currentQuestion.getChosenAnswerIndex()) {
                            answers.get(i).getBackground().setColorFilter(ContextCompat.getColor(
                                    answers.get(i).getContext(), R.color.colorPrimary),
                                    PorterDuff.Mode.MULTIPLY);
                        } else {
                            answers.get(i).getBackground().setColorFilter(ContextCompat.getColor(
                                    answers.get(i).getContext(), R.color.blue_violet),
                                    PorterDuff.Mode.MULTIPLY);
                        }
                    } else {
                        answers.get(i).setVisibility(View.GONE);
                    }
                }
            }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        viewModel.getGame(this.getContext()).observe(this, observer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_game, container, false);
        questionText = view.findViewById(R.id.textView);
        answers.add((Button) view.findViewById(R.id.textView2));
        answers.add((Button) view.findViewById(R.id.textView3));
        answers.add((Button) view.findViewById(R.id.textView4));
        answers.add((Button) view.findViewById(R.id.textView5));
        progress = view.findViewById(R.id.gameProgress);
        for (int i = 0; i < 4; i++) {
            final int finalI = i;
            answers.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentGame.getQuestions().get(questionIndex).setChosenAnswerIndex(finalI);
                    if (questionIndex < 4) {
                        questionIndex += 1;
                    } else {
                        currentGame.setComplete(true);
                        int scoreCalc = 0;
                        for (Question question : currentGame.getQuestions()) {
                            List<String> answers = AnswersUtil.orderAnswers(question);
                            if (answers.get(question.getChosenAnswerIndex()).equals(question.getCorrectAnswer())) {
                                scoreCalc += 1;
                            }
                            question.setChosenAnswer(answers.get(question.getChosenAnswerIndex()));
                        }
                        currentGame.setScore(scoreCalc);
                        NavController navController = NavHostFragment.findNavController(GameFragment.this);
                        Bundle bundle =  new Bundle();
                        bundle.putSerializable("Game", currentGame);
                        navController.navigate(R.id.action_gameFragment_to_resultsFragment, bundle);
                    }
                    viewModel.saveGame(GameFragment.this.getContext(), currentGame);
                }
            });
        }
        return view;
    }

}
