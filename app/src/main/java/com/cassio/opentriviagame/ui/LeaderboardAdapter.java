package com.cassio.opentriviagame.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cassio.opentriviagame.R;
import com.cassio.opentriviagame.entity.Game;

import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class LeaderboardAdapter extends RecyclerView.Adapter<LeaderboardAdapter.MyViewHolder>{
    private List<Game> mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public ConstraintLayout layout;
        public MyViewHolder(ConstraintLayout v) {
            super(v);
            layout = v;
        }
    }

    public LeaderboardAdapter(List<Game> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LeaderboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.main_line_view, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TextView tv1 = holder.layout.findViewById(R.id.leaderboardPlayerName);
        tv1.setText(mDataset.get(position).getPlayer_name());
        TextView tv2 = holder.layout.findViewById(R.id.leaderboardScore);
        int score = mDataset.get(position).getScore();
        tv2.setText("" + score);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
