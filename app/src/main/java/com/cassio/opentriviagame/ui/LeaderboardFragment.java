package com.cassio.opentriviagame.ui;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cassio.opentriviagame.R;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.viewmodel.LeaderboardViewModel;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class LeaderboardFragment extends androidx.fragment.app.Fragment {
    private View view;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private LeaderboardViewModel viewModel;

    public LeaderboardFragment() {
        // Required empty public constructor
    }

    private final Observer<List<Game>> observer = new Observer<List<Game>>() {
        @Override
        public void onChanged(List<Game> games) {
            if (games != null && games.size() > 0) {
                mRecyclerView =  view.findViewById(R.id.leaderboardRecyclerView);
                mRecyclerView.setHasFixedSize(true);

                mLayoutManager = new LinearLayoutManager(LeaderboardFragment.this.getContext());
                mRecyclerView.setLayoutManager(mLayoutManager);

                mAdapter = new LeaderboardAdapter(games);
                mRecyclerView.setAdapter(mAdapter);
            } else {
            }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(LeaderboardViewModel.class);
        viewModel.getTop10(this.getContext()).observe(this, observer);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_leaderboard, container, false);
        return view;
    }
}
