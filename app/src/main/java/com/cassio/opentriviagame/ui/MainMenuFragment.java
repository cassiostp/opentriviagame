package com.cassio.opentriviagame.ui;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;

import com.cassio.opentriviagame.R;
import com.cassio.opentriviagame.entity.Player;
import com.cassio.opentriviagame.utilities.PlayerUtil;
import com.cassio.opentriviagame.viewmodel.MainMenuViewModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

public class MainMenuFragment extends Fragment {
    private AutoCompleteTextView playerName;

    public MainMenuFragment() {
        // Required empty public constructor
    }

    private MainMenuViewModel viewModel;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(MainMenuViewModel.class);
        final Observer<List<Player>> observer = new Observer<List<Player>>() {
            @Override
            public void onChanged(List<Player> players) {
                ArrayAdapter<String> playerNameAdapter =
                        new ArrayAdapter<>(MainMenuFragment.this.getContext(),
                                android.R.layout.simple_list_item_1, PlayerUtil.safeGetPlayersNames(players));
                playerName.setAdapter(playerNameAdapter);
            }
        };
        viewModel.getPlayers(this.getContext()).observe(this, observer);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        playerName = view.findViewById(R.id.player_name);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle someNewInfoBundle = new Bundle();
        someNewInfoBundle.putString("string", "This is a string");
        final Button button = view.findViewById(R.id.button);
        button.setOnClickListener(savePlayerAndNavigate());
        playerName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                enablePlayButton(button);
            }
        });
        ImageButton trophy = view.findViewById(R.id.leaderboardButton);
        trophy.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_mainMenuFragment_to_leaderboardFragment));
    }

    private View.OnClickListener savePlayerAndNavigate() {
        final Fragment fragment = this;
        final Context context = fragment.getContext();
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Player player = new Player(playerName.getText().toString());
                viewModel.storeNewPlayer(context, player);
                NavController navController = NavHostFragment.findNavController(fragment);
                navController.navigate(R.id.action_mainMenuFragment_to_gameFragment);
            }
        };
    }

    private void enablePlayButton(Button button) {
        if (playerName.getText().toString().length() > 0) {
            button.setEnabled(true);
        } else {
            button.setEnabled(false);
        }
    }
}
