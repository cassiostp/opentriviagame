package com.cassio.opentriviagame.ui;


import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cassio.opentriviagame.R;
import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.entity.Question;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;

public class ResultsFragment extends androidx.fragment.app.Fragment {

    Game finishedGame;

    public ResultsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        finishedGame = (Game) getArguments().getSerializable("Game");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Resources res = getResources();
        String playerName = finishedGame.getPlayer_name();
        List<Question> questions = finishedGame.getQuestions();
        String thanksText = String.format(res.getString(R.string.thanks), playerName);
        TextView resultsThanks = view.findViewById(R.id.resultsThanks);
        resultsThanks.setText(thanksText);

        TextView question1 = view.findViewById(R.id.question);
        question1.setText(String.format(res.getString(R.string.question), questions.get(0).getQuestion()));
        TextView question2 = view.findViewById(R.id.question2);
        question2.setText(String.format(res.getString(R.string.question), questions.get(1).getQuestion()));
        TextView question3 = view.findViewById(R.id.question3);
        question3.setText(String.format(res.getString(R.string.question), questions.get(2).getQuestion()));
        TextView question4 = view.findViewById(R.id.question4);
        question4.setText(String.format(res.getString(R.string.question), questions.get(3).getQuestion()));
        TextView question5 = view.findViewById(R.id.question5);
        question5.setText(String.format(res.getString(R.string.question), questions.get(4).getQuestion()));

        TextView chosenAnswer = view.findViewById(R.id.chosenAnswer);
        chosenAnswer.setText(String.format(res.getString(R.string.chosenAnswer), questions.get(0).getChosenAnswer()));
        TextView chosenAnswer2 = view.findViewById(R.id.chosenAnswer2);
        chosenAnswer2.setText(String.format(res.getString(R.string.chosenAnswer), questions.get(1).getChosenAnswer()));
        TextView chosenAnswer3 = view.findViewById(R.id.chosenAnswer3);
        chosenAnswer3.setText(String.format(res.getString(R.string.chosenAnswer), questions.get(2).getChosenAnswer()));
        TextView chosenAnswer4 = view.findViewById(R.id.chosenAnswer4);
        chosenAnswer4.setText(String.format(res.getString(R.string.chosenAnswer), questions.get(3).getChosenAnswer()));
        TextView chosenAnswer5 = view.findViewById(R.id.chosenAnswer5);
        chosenAnswer5.setText(String.format(res.getString(R.string.chosenAnswer), questions.get(4).getChosenAnswer()));

        TextView correctAnswer = view.findViewById(R.id.correctAnswer);
        correctAnswer.setText(String.format(res.getString(R.string.correctAnswer), questions.get(0).getCorrectAnswer()));
        TextView correctAnswer2 = view.findViewById(R.id.correctAnswer2);
        correctAnswer2.setText(String.format(res.getString(R.string.correctAnswer), questions.get(1).getCorrectAnswer()));
        TextView correctAnswer3 = view.findViewById(R.id.correctAnswer3);
        correctAnswer3.setText(String.format(res.getString(R.string.correctAnswer), questions.get(2).getCorrectAnswer()));
        TextView correctAnswer4 = view.findViewById(R.id.correctAnswer4);
        correctAnswer4.setText(String.format(res.getString(R.string.correctAnswer), questions.get(3).getCorrectAnswer()));
        TextView correctAnswer5 = view.findViewById(R.id.correctAnswer5);
        correctAnswer5.setText(String.format(res.getString(R.string.correctAnswer), questions.get(4).getCorrectAnswer()));

        TextView score = view.findViewById(R.id.score);
        score.setText(String.format(res.getString(R.string.score), finishedGame.getScore()));

        Button returnButton = view.findViewById(R.id.returnButton);
        returnButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_resultsFragment_to_mainMenuFragment));
    }
}
