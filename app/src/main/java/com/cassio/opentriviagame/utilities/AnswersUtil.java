package com.cassio.opentriviagame.utilities;

import com.cassio.opentriviagame.entity.Question;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AnswersUtil {
    public static List<String> orderAnswers(Question currentQuestion) {
        List<String> answersText = currentQuestion.getIncorrectAnswers();
        answersText.add(currentQuestion.getCorrectAnswer());
        // remove possible duplicates by transferring data into a hash set and back
        Set<String> duplicatesHelper = new HashSet<>();
        duplicatesHelper.addAll(answersText);
        answersText.clear();
        answersText.addAll(duplicatesHelper);
        // making the answer ordering predictable
        Collections.sort(answersText, String.CASE_INSENSITIVE_ORDER);
        return answersText;
    }
}
