package com.cassio.opentriviagame.utilities;

import com.cassio.opentriviagame.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerUtil {
    public static List<String> safeGetPlayersNames(List<Player> players) {
        List<String> playerNames = new ArrayList<>();
        if (players == null || (players != null && players.size() == 0)) {
            return playerNames;
        }
        for (Player player : players) {
            playerNames.add(player.getName());
        }
        return playerNames;
    }
}
