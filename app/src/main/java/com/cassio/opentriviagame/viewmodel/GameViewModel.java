package com.cassio.opentriviagame.viewmodel;

import android.content.Context;
import android.util.Log;

import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.repository.GameRepository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class GameViewModel extends ViewModel {
    private LiveData<Game> game;
    private GameRepository gameRepo = new GameRepository();

    public LiveData<Game> getGame(Context context) {
        game = gameRepo.getCurrentGame(context);
        return game;
    }

    public void setNewGame(Context context) {
        gameRepo.setNewGame(context);
    }

    public void saveGame(Context context, Game game) {
        gameRepo.saveGame(context, game);
    }
}
