package com.cassio.opentriviagame.viewmodel;

import android.content.Context;

import com.cassio.opentriviagame.entity.Game;
import com.cassio.opentriviagame.repository.GameRepository;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class LeaderboardViewModel extends ViewModel {
    private GameRepository gameRepo = new GameRepository();

    public LiveData<List<Game>> getTop10 (Context context) {
        return gameRepo.getBestGames(context);
    }
}
