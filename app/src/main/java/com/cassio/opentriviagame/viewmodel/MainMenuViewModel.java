package com.cassio.opentriviagame.viewmodel;

import android.content.Context;

import com.cassio.opentriviagame.entity.Player;
import com.cassio.opentriviagame.repository.PlayerRepository;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class MainMenuViewModel extends ViewModel {
    private PlayerRepository playerRepository = new PlayerRepository();

    public LiveData<List<Player>> getPlayers(Context context) {
        return playerRepository.getPlayers(context);
    }

    public void storeNewPlayer(Context context, Player player) {
        playerRepository.insertPlayer(context, player);
    }
}
