package com.cassio.opentriviagame.db;

import com.cassio.opentriviagame.entity.Question;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class QuestionsConvertersTest {
    private String stringReturn;

    private List<Question> generateList() {
        List<Question> questions = new ArrayList<>();
        Question question1 = new Question();
        question1.setQuestion("test1");
        Question question2 = new Question();
        question1.setQuestion("test2");
        Question question3 = new Question();
        question1.setQuestion("test3");
        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        return questions;
    }
    private void prepareTest() {
        List<Question> questions = generateList();
        stringReturn = QuestionsConverters.fromArrayList((ArrayList<Question>) questions);
    }

    @Test
    public void fromString() {
        prepareTest();
        System.out.println(QuestionsConverters.fromString(stringReturn));
        System.out.println(generateList());
        assertEquals(QuestionsConverters.fromString(stringReturn).size(), generateList().size());
    }

    @Test
    public void fromArrayList() {
        prepareTest();
        assertEquals(QuestionsConverters.fromArrayList((ArrayList<Question>) generateList()), stringReturn);
    }
}