package com.cassio.opentriviagame.utilities;

import com.cassio.opentriviagame.entity.Question;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AnswersUtilTest {

    @Test
    public void orderAnswers() {
        List<String> strings = new ArrayList<>();
        strings.add("FALSE");
        Question question = new Question();
        question.setCorrectAnswer("TRUE");
        question.setIncorrectAnswers(strings);
        assertEquals("FALSE", AnswersUtil.orderAnswers(question).get(0));
        assertEquals("TRUE", AnswersUtil.orderAnswers(question).get(1));
    }
}