package com.cassio.opentriviagame.utilities;

import com.cassio.opentriviagame.entity.Player;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PlayerUtilTest {

    @Test
    public void safeGetPlayersNames() {
        List<Player> players = new ArrayList<>();
        List<Player> nullPlayers = null;
        List<String> result = new ArrayList<>();
        result.add("test1");
        result.add("test2");
        result.add("test3");
        players.add(new Player(result.get(0)));
        players.add(new Player(result.get(1)));
        players.add(new Player(result.get(2)));

        assertEquals(result, PlayerUtil.safeGetPlayersNames(players));
        assertTrue(PlayerUtil.safeGetPlayersNames(nullPlayers) != null);
    }
}